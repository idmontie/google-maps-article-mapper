////
////  Test Code
////

$(function () {
  var apiKey = "AIzaSyCELxGXOiZWGiaBO2BGOCWd4POwvAG9I7k";

  $('#map-canvas').mapArticles([
      {
          title : "Test Article 1",
          short_description : "This is the first test article for Article Mapper.",
          lat : "33.464092",
          long : "-111.923112"
      },
      {
          title : "Really Cool Stuff",
          short_description : "This is the first test article for Article Mapper.",
          lat : "33.421481",
          long : "-111.935329"
      }
   ], {
    center : $.mapArticles.LOCATIONS.ARIZONA_CENTER,
    zoom : 7
   },apiKey);  
})

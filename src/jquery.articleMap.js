////
////  Article Mapper
////
////  Map articles to a map.
////  Ivan Montiel
////

(function ( $ ) {
    // Register key geographical centers
    $.mapArticles = {
        LOCATIONS : {
            UNITED_STATES_CONTIGUOUS_CENTER : ["39.833333", "-98.583333"],
            UNITED_STATES_CENTER : ["44.966667", "-103.766667"],
            NORTH_AMERICA_CENTER : ["48.367222", "-99.996111"],
            ARIZONA_CENTER : ["33.990581", "-111.994294"],
            WORLD_CENTER_1 : ["32.175612", "-37.529297"]
        }
    };

    var defaults = {
        zoom : 2,
        center : $.mapArticles.LOCATIONS.WORLD_CENTER_1
    };


    // Register ourselves with jQuery as a plugin
    $.fn.mapArticles = function (articles, options, apiKey) {
        function loadScript(apiKey) {
          var script = document.createElement('script');
          script.type = 'text/javascript';
          script.src = 'https://maps.googleapis.com/maps/api/js?key=' + apiKey + '&' +
              'callback=_gmapinitialize';
          document.body.appendChild(script);
        }

        window._gmapinitialize = function () {
            var params = $.extend({}, defaults, options);

            // Parse center
            var center = [parseFloat(params.center[0]), 
                          parseFloat(params.center[1])];

            var mapOptions = {
                center: new google.maps.LatLng(center[0], center[1]),
                zoom: params.zoom,
                mapTypeId: google.maps.MapTypeId.HYBRID
            };
            var map = new google.maps.Map(document.getElementById("map-canvas"),
                                          mapOptions);
            
            for (var i = 0; i < articles.length; i++) {
                var article = articles[i];
                var lat = parseFloat(article.lat);
                var long = parseFloat(article.long);
                console.log(lat, long);
                var gLatlng = new google.maps.LatLng(lat, long);
                
                var infowindow = new google.maps.InfoWindow();
                
                var marker = new google.maps.Marker({
                    position: gLatlng,
                    title: article.title,
                    map : map
                });
                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        var template = '<div><h1>{{title}}</h1><p>{{short_description}}</p></div>';
                        template = template.replace('{{title}}',  articles[i].title);   
                        template = template.replace('{{short_description}}',  articles[i].short_description);
                            
                        
                        infowindow.setContent(template);
                        infowindow.open(map, marker);
                     }
                })(marker, i));
            }
           
        }
        
        loadScript(apiKey);
    }
}( jQuery ));